import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:murtikar/screens/favourites.dart';
import 'package:provider/provider.dart';
import 'package:murtikar/models/product_model.dart';
import 'package:murtikar/provider/user_provider.dart';
import 'package:murtikar/screens/cart_screen.dart';
import 'package:murtikar/screens/login.dart';
import 'package:murtikar/widgets/featured_card.dart';
import 'package:murtikar/widgets/product_card.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<ProductModel> _productModel = List<ProductModel>();
  List<ProductModel> _featuredModel = List<ProductModel>();
  var products_list = [];
  var user, data;
  @override
  void initState() {
    super.initState();
    getProductsData().then((productsList) {
      setState(() {
        _productModel.addAll(productsList);
      });
    });
    getFeaturedProductsData().then((featuredList) {
      _featuredModel.addAll(featuredList);
    });
  }

  Future<List<ProductModel>> getProductsData() async {
    var productmodel = List<ProductModel>();
    //query database
    final QuerySnapshot result =
        await Firestore.instance.collection('products').getDocuments();
    //list of snapshots
    final List<DocumentSnapshot> documents = result.documents;
    // iterate snapshots for data
    for (int i = 0; i < documents.length; i++) {
      data = documents[i].data;
      products_list.add(data);
    }
    //iterate each product
    for (var productModelJson in products_list) {
      productmodel.add(ProductModel.fromJson(productModelJson));
    }
    return productmodel;
  }

  Future<List<ProductModel>> getFeaturedProductsData() async {
    var productmodel = List<ProductModel>();
    var featured_list = [];
    //query database
    final QuerySnapshot result = await Firestore.instance
        .collection('products')
        .where("featured", isEqualTo: true)
        .getDocuments();
    //list of snapshots
    final List<DocumentSnapshot> documents = result.documents;
    // iterate snapshots for data
    for (int i = 0; i < documents.length; i++) {
      data = documents[i].data;
      featured_list.add(data);
    }
    //iterate each product
    for (var productModelJson in featured_list) {
      productmodel.add(ProductModel.fromJson(productModelJson));
    }
    return productmodel;
  }

  Widget image_carousel = Container(
    height: 200.0,
    child: Carousel(
      boxFit: BoxFit.cover,
      images: [
        ExactAssetImage('assets/images/c1.jpg'),
        ExactAssetImage('assets/images/c2.jpg'),
        ExactAssetImage('assets/images/c3.jpg'),
        ExactAssetImage('assets/images/c4.jpg'),
        ExactAssetImage('assets/images/c5.jpg'),
      ],
      dotSize: 4.0,
      autoplay: true,
      animationCurve: Curves.fastOutSlowIn,
      animationDuration: Duration(seconds: 1),
    ),
  );
  @override
  Widget build(BuildContext context) {
    UserProvider user = Provider.of<UserProvider>(context);
    var userId = user.user.uid;

    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text("Murtikar"),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.search), onPressed: () {}),
          IconButton(
              icon: Icon(Icons.shopping_cart),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CartScreen(
                              uid: userId,
                            )));
              })
        ],
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            // Drawer Header
            StreamBuilder<DocumentSnapshot>(
              stream: Firestore.instance
                  .collection('users')
                  .document(user.user.uid)
                  .snapshots(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  var userData = snapshot.data;
                  print(userData["name"]);
                  return UserAccountsDrawerHeader(
                      accountName: Text(userData["name"]),
                      accountEmail: Text(userData["email"]),
                      currentAccountPicture: CircleAvatar(
                        backgroundColor: Colors.white,
                        child: Icon(Icons.person),
                      ));
                }
                return Container();
              },
            ),
            InkWell(
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => HomePage()));
              },
              child: ListTile(
                title: Text("Home Page"),
                leading: Icon(
                  Icons.home,
                  color: Color(0xff58b4ae),
                ),
              ),
            ),
            InkWell(
              onTap: () {},
              child: ListTile(
                title: Text("My account"),
                leading: Icon(
                  Icons.person,
                  color: Color(0xff58b4ae),
                ),
              ),
            ),
            InkWell(
              onTap: () {},
              child: ListTile(
                title: Text("My Orders"),
                leading: Icon(
                  Icons.shopping_basket,
                  color: Color(0xff58b4ae),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CartScreen(
                              uid: userId,
                            )));
              },
              child: ListTile(
                title: Text("Shopping Cart"),
                leading: Icon(
                  Icons.shopping_cart,
                  color: Color(0xff58b4ae),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Favourites(
                              uid: userId,
                            )));
              },
              child: ListTile(
                title: Text("Favourites"),
                leading: Icon(
                  Icons.favorite,
                  color: Color(0xff58b4ae),
                ),
              ),
            ),
            Divider(),
            InkWell(
              onTap: () {},
              child: ListTile(
                title: Text("Settings"),
                leading: Icon(
                  Icons.settings,
                  color: Color(0xff58b4ae),
                ),
              ),
            ),
            InkWell(
              onTap: () {},
              child: ListTile(
                title: Text("About"),
                leading: Icon(
                  Icons.help,
                  color: Colors.blue,
                ),
              ),
            ),
            InkWell(
              onTap: () {
                FirebaseAuth.instance.signOut().then((value) {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => Login()));
                });
              },
              child: ListTile(
                title: Text("Logout"),
                leading: Icon(
                  Icons.help,
                  color: Colors.blue,
                ),
              ),
            ),
          ],
        ),
      ),
      body: Column(
        children: <Widget>[
          // image_carousel,
          // new Padding(
          //   padding: const EdgeInsets.all(4.0),
          //   child: Container(
          //       alignment: Alignment.centerLeft, child: new Text('Categories')),
          // ),

          //Horizontal list view begins here
          // HorizontalList(),
          Padding(
            padding: const EdgeInsets.all(14.0),
            child: Container(
                alignment: Alignment.centerLeft,
                child: new Text('Featured products')),
          ),
          // FeaturedProducts(),

          Row(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(14.0),
                  child: Container(
                      height: 230,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: _featuredModel.length,
                          itemBuilder: (context, index) {
                            return FeaturedCard(
                                uid: userId,
                                pid: _featuredModel[index].pid,
                                name: _featuredModel[index].name,
                                price: _featuredModel[index].price,
                                picture: _featuredModel[index].images[0],
                                brand: _featuredModel[index].brand,
                                onSale: _featuredModel[index].sale);
                          })),
                ),
              ),
            ],
          ),

          Padding(
            padding: const EdgeInsets.all(14.0),
            child: Container(
                alignment: Alignment.centerLeft,
                child: new Text('Recent products')),
          ),

          Expanded(
            child: ListView.builder(
              itemCount: _productModel.length,
              itemBuilder: (context, index) {
                return ProductCard(
                    uid: userId,
                    pid: _productModel[index].pid,
                    name: _productModel[index].name,
                    price: _productModel[index].price,
                    picture: _productModel[index].images[0],
                    brand: _productModel[index].brand,
                    onSale: _productModel[index].sale);
              },
            ),
          ),

          //grid view
          // Flexible(
          //   child: Products(),
          // )
        ],
      ),
    );
  }
}
