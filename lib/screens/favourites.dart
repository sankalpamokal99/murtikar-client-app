import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:murtikar/models/fav_product_model.dart';
import 'package:murtikar/screens/cart_screen.dart';
import 'package:murtikar/widgets/favourite_product.dart';

class Favourites extends StatefulWidget {
  final String uid;

  Favourites({this.uid});
  @override
  _FavouritesState createState() => _FavouritesState();
}

class _FavouritesState extends State<Favourites> {
  List<FavProductModel> _favProductModel = List<FavProductModel>();
  var fav_products = [];
  var data;
  @override
  void initState() {
    super.initState();
    getFavProductsData(widget.uid).then((favproductsList) {
      setState(() {
        _favProductModel.addAll(favproductsList);
      });
    });
  }

  Future<List<FavProductModel>> getFavProductsData(String userId) async {
    var favproductmodel = List<FavProductModel>();
    //query database
    final QuerySnapshot result = await Firestore.instance
        .collection('users')
        .document(userId)
        .collection('fav')
        .getDocuments();
    //list of snapshots
    final List<DocumentSnapshot> documents = result.documents;
    // iterate snapshots for data
    for (int i = 0; i < documents.length; i++) {
      data = documents[i].data;
      fav_products.add(data);
    }

    // iterate each product
    for (var favproductModelJson in fav_products) {
      favproductmodel.add(FavProductModel.fromJson(favproductModelJson));
    }
    return favproductmodel;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          title: Text("Favourites"),
          actions: <Widget>[
            IconButton(icon: Icon(Icons.search), onPressed: () {}),
            IconButton(
                icon: Icon(Icons.shopping_cart),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CartScreen(
                                uid: widget.uid,
                              )));
                })
          ],
        ),
        body: Column(children: <Widget>[
          Expanded(
            child: ListView.builder(
              itemCount: _favProductModel.length,
              itemBuilder: (context, index) {
                return FavouriteProduct(
                  userid: widget.uid,
                  fav_prod_id: _favProductModel[index].pid,
                  fav_prod_name: _favProductModel[index].name,
                  fav_prod_picture: _favProductModel[index].picture,
                  fav_prod_price: _favProductModel[index].price,
                  fav_prod_brand: _favProductModel[index].brand,
                );
              },
            ),
          )
        ]));
  }
}
