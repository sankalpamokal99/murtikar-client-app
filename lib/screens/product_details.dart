import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'cart_screen.dart';

class ProductDetails extends StatefulWidget {
  final String name;
  final double price;
  final String picture;
  final String brand;
  final bool onSale;
  final String uid;
  final String pid;

  ProductDetails(
      {@required this.uid,
      @required this.pid,
      @required this.name,
      @required this.price,
      @required this.picture,
      @required this.brand,
      @required this.onSale});

  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  var _exists;
  @override
  void initState() {
    super.initState();
    _exists = checkIfFav();
    // fav = checkIfFav();
  }

  Future<DocumentSnapshot> checkIfFav() async {
    return await Firestore.instance
        .collection('users')
        .document(widget.uid)
        .collection('fav')
        .document(widget.pid)
        .get();
  }

  addToFav() async {
    await Firestore.instance
        .collection('users')
        .document(widget.uid)
        .collection('fav')
        .document(widget.pid)
        .setData({
      'uid': widget.uid,
      'pid': widget.pid,
      'price': widget.price,
      'picture': widget.picture,
      'name': widget.name,
      'brand': widget.brand,
    });
    return true;
  }

  removeFromFav(String uuid, String ppid) async {
    await Firestore.instance
        .collection('users')
        .document(uuid)
        .collection('fav')
        .document(ppid)
        .delete();
    return true;
  }

  bool updateQty(DocumentSnapshot snapshot) {
    var qty = 1;
    var oldQty = 1;
    oldQty = snapshot.data['qty'];
    qty = oldQty + 1;
    Firestore.instance
        .collection('users')
        .document(widget.uid)
        .collection('cart')
        .document(widget.pid)
        .updateData({'qty': qty});
    return true;
  }

  bool createCartItem(DocumentSnapshot snapshot) {
    Firestore.instance
        .collection('users')
        .document(widget.uid)
        .collection('cart')
        .document(widget.pid)
        .setData({
      'uid': widget.uid,
      'pid': widget.pid,
      'price': widget.price,
      'picture': widget.picture,
      'name': widget.name,
      'brand': widget.brand,
      'qty': 1,
      'sale': widget.onSale,
    });
    return true;
  }

  Future addProductToCart() async {
    try {
      await Firestore.instance
          .collection('users')
          .document(widget.uid)
          .collection('cart')
          .document(widget.pid)
          .get()
          .then((snapshot) {
        if (snapshot.exists) {
          return updateQty(snapshot);
        } else {
          return createCartItem(snapshot);
        }
      });
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    height: 275.0,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: NetworkImage(widget.picture),
                            fit: BoxFit.cover)),
                  ),
                  GestureDetector(
                    child: Container(
                      height: 275.0,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.transparent,
                    ),
                    onTap: () {},
                  ),
                  GestureDetector(
                    child: Container(
                      height: 275.0,
                      width: MediaQuery.of(context).size.width / 2,
                      color: Colors.transparent,
                    ),
                    onTap: () {},
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        IconButton(
                            icon: Icon(Icons.arrow_back),
                            color: Colors.black,
                            onPressed: () {
                              Navigator.of(context).pop();
                            }),
                        Material(
                            elevation: 4.0,
                            borderRadius: BorderRadius.circular(20.0),
                            child: Container(
                                height: 40.0,
                                width: 40.0,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20.0)),
                                child: FutureBuilder<DocumentSnapshot>(
                                    future: _exists,
                                    builder: (context, snapshot) {
                                      if (snapshot.connectionState ==
                                          ConnectionState.done) {
                                        if (snapshot.data.exists) {
                                          return InkWell(
                                            onTap: () {
                                              removeFromFav(
                                                  widget.uid, widget.pid);
                                              setState(() {
                                                _exists = checkIfFav();
                                              });
                                            },
                                            child: Icon(
                                              Icons.favorite,
                                              color: Colors.red,
                                            ),
                                          );
                                        } else {
                                          return InkWell(
                                            onTap: () {
                                              addToFav();
                                              setState(() {
                                                _exists = checkIfFav();
                                              });
                                            },
                                            child: Icon(
                                              Icons.favorite_border,
                                              color: Colors.red,
                                            ),
                                          );
                                        }
                                      }
                                      return SizedBox.shrink();
                                    })))
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20.0),
              Padding(
                padding: EdgeInsets.only(left: 15.0),
                child: Text(
                  'Product Id: ${widget.pid}',
                  style: TextStyle(fontSize: 15.0, color: Colors.grey),
                ),
              ),
              SizedBox(height: 10.0),
              Padding(
                padding: const EdgeInsets.only(left: 15.0),
                child: Text(
                  '${widget.name}',
                  style: TextStyle(
                      fontSize: 25.0,
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(height: 10.0),
              Padding(
                  padding: EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: (MediaQuery.of(context).size.width / 2),
                        child: Text(
                          'Short description',
                          style: TextStyle(
                            fontSize: 12.0,
                            color: Colors.grey.withOpacity(0.8),
                          ),
                        ),
                      ),
                      Text(
                        '\$ ${widget.price}',
                        style: TextStyle(
                            fontSize: 25.0,
                            color: Colors.red,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  )),
              SizedBox(height: 20.0),
              Padding(
                padding: EdgeInsets.only(left: 15.0),
                child: Text(
                  'Color',
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.black,
                  ),
                ),
              ),
              SizedBox(height: 20.0),
              Padding(
                  padding: EdgeInsets.only(left: 15.0),
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: 20.0,
                        width: 20.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25.0),
                            color: Colors.red),
                      ),
                      SizedBox(width: 15.0),
                      Container(
                        height: 20.0,
                        width: 20.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25.0),
                            color: Colors.green),
                      ),
                      SizedBox(width: 15.0),
                      Container(
                        height: 20.0,
                        width: 20.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25.0),
                            color: Colors.blue),
                      )
                    ],
                  )),
              SizedBox(height: 20.0),
              Padding(
                padding: EdgeInsets.only(left: 15.0),
                child: Text(
                  'Description',
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.black,
                  ),
                ),
              ),
              SizedBox(height: 20.0),
            ],
          )
        ],
      ),
      bottomNavigationBar: Material(
          elevation: 7.0,
          color: Colors.white,
          child: Container(
              height: 50.0,
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(width: 10.0),
                    InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => CartScreen(
                                  uid: widget.uid,
                                )));
                      },
                      child: Container(
                        height: 50.0,
                        width: 50.0,
                        color: Colors.white,
                        child: Icon(
                          Icons.shopping_cart,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        addProductToCart();
                      },
                      child: Container(
                          color: Color(0xff02c39a),
                          width: MediaQuery.of(context).size.width - 130.0,
                          child: Center(
                              child: Text(
                            'Add to Cart',
                            style: TextStyle(
                                fontSize: 18.0,
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ))),
                    )
                  ]))),
    );
  }
}
