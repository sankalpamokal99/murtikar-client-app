import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart'; //no need
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:murtikar/db/users.dart'; //no need
import 'package:murtikar/provider/user_provider.dart';
import 'package:murtikar/screens/login.dart';
import 'package:murtikar/widgets/loading.dart';
import 'home.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final _formKey = GlobalKey<FormState>();
  final _key = GlobalKey<ScaffoldState>();
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  UserServices _userServices = UserServices();

  TextEditingController _nameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _mobileController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _confirmpasswordController = TextEditingController();
  bool hidePass = true;

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserProvider>(context);

    return Scaffold(
      key: _key,
      body: user.status == Status.Authenticating
          ? Loading()
          : Stack(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(16),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey[350],
                          blurRadius:
                              20.0, // has the effect of softening the shadow
                        )
                      ],
                    ),
                    child: Form(
                        key: _formKey,
                        child: ListView(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Container(
                                  alignment: Alignment.topCenter,
                                  child: SvgPicture.asset(
                                    'assets/images/sign_up.svg',
                                    width: 200.0,
                                  )),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  14.0, 8.0, 14.0, 8.0),
                              child: Padding(
                                padding: const EdgeInsets.only(left: 12.0),
                                child: SizedBox(
                                  height: 50.0,
                                  child: TextFormField(
                                    controller: _nameController,
                                    decoration: InputDecoration(
                                        prefixIcon: Icon(Icons.person_outline),
                                        fillColor: Colors.grey.withOpacity(0.2),
                                        filled: true,
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                          const Radius.circular(8.0),
                                        )),
                                        hintText: "Full Name"),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "The name field cannot be empty";
                                      }
                                      return null;
                                    },
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  14.0, 8.0, 14.0, 8.0),
                              child: Padding(
                                padding: const EdgeInsets.only(left: 12.0),
                                child: SizedBox(
                                  height: 50.0,
                                  child: TextFormField(
                                    keyboardType: TextInputType.phone,
                                    controller: _mobileController,
                                    decoration: InputDecoration(
                                        prefixIcon: Icon(Icons.phone_android),
                                        fillColor: Colors.grey.withOpacity(0.2),
                                        filled: true,
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                          const Radius.circular(8.0),
                                        )),
                                        hintText: "Mobile"),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "Mobile number is required";
                                      }
                                      return null;
                                    },
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  14.0, 8.0, 14.0, 8.0),
                              child: Padding(
                                padding: const EdgeInsets.only(left: 12.0),
                                child: SizedBox(
                                  height: 50.0,
                                  child: TextFormField(
                                    controller: _emailController,
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.alternate_email),
                                      fillColor: Colors.grey.withOpacity(0.2),
                                      filled: true,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                        const Radius.circular(8.0),
                                      )),
                                      hintText: "Email",
                                    ),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        Pattern pattern =
                                            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                        RegExp regex = new RegExp(pattern);
                                        if (!regex.hasMatch(value))
                                          return 'Please make sure your email address is valid';
                                        else
                                          return null;
                                      }
                                    },
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  14.0, 8.0, 14.0, 8.0),
                              child: Padding(
                                padding: const EdgeInsets.only(left: 12.0),
                                child: SizedBox(
                                  height: 50.0,
                                  child: TextFormField(
                                    controller: _passwordController,
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.lock_outline),
                                      suffixIcon: IconButton(
                                          icon: Icon(Icons.remove_red_eye),
                                          onPressed: () {
                                            setState(() {
                                              hidePass = false;
                                            });
                                          }),
                                      fillColor: Colors.grey.withOpacity(0.2),
                                      filled: true,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                        const Radius.circular(8.0),
                                      )),
                                      hintText: "Password",
                                    ),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "The password field cannot be empty";
                                      } else if (value.length < 6) {
                                        return "the password has to be at least 6 characters long";
                                      }
                                      return null;
                                    },
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  14.0, 8.0, 14.0, 8.0),
                              child: Padding(
                                padding: const EdgeInsets.only(left: 12.0),
                                child: SizedBox(
                                  height: 50.0,
                                  child: TextFormField(
                                    obscureText: hidePass,
                                    controller: _confirmpasswordController,
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.lock_outline),
                                      suffixIcon: IconButton(
                                          icon: Icon(Icons.remove_red_eye),
                                          onPressed: () {
                                            setState(() {
                                              hidePass = false;
                                            });
                                          }),
                                      fillColor: Colors.grey.withOpacity(0.2),
                                      filled: true,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                        const Radius.circular(8.0),
                                      )),
                                      hintText: "Confirm Password",
                                    ),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "Enter similar password";
                                      } else if (value !=
                                          _passwordController.text) {
                                        return 'Password doesn\'t match';
                                      }
                                      return null;
                                    },
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  100.0, 8.0, 100.0, 8.0),
                              child: Material(
                                  borderRadius: BorderRadius.circular(20.0),
                                  color: Colors.black,
                                  elevation: 0.0,
                                  child: SizedBox(
                                    height: 45.0,
                                    width: 100.0,
                                    child: MaterialButton(
                                      onPressed: () async {
                                        // validateForm();
                                        if (_formKey.currentState.validate()) {
                                          if (!await user.signUp(
                                              _nameController.text,
                                              _emailController.text,
                                              _mobileController.text,
                                              _passwordController.text)) {
                                            _key.currentState.showSnackBar(
                                                SnackBar(
                                                    content: Text(
                                                        "Sign up failed")));
                                            return;
                                          }
                                          Navigator.of(context).pushReplacement(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      HomePage()));
                                        }
                                      },
                                      child: Text(
                                        "Sign up",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20.0),
                                      ),
                                    ),
                                  )),
                            ),
                            Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: InkWell(
                                    onTap: () {
                                      _formKey.currentState.reset();
                                      Navigator.pushReplacement(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => Login()));
                                    },
                                    child: Text(
                                      "Already have an account?",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 16),
                                    ))),
                          ],
                        )),
                  ),
                ),
              ],
            ),
    );
  }

  Future validateForm() async {
    FormState formState = _formKey.currentState;
    if (formState.validate()) {
      FirebaseUser user = await firebaseAuth.currentUser();
      if (user == null) {
        firebaseAuth
            .createUserWithEmailAndPassword(
                email: _emailController.text,
                password: _passwordController.text)
            .then((user) => {
                  _userServices.createUser({
                    "userid": user.user.uid,
                    "name": _nameController.text,
                    "email": _emailController.text,
                    "mobile": _mobileController.text,
                    "password": _passwordController.text
                  })
                })
            .catchError((e) => {print(e.toString())});
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => HomePage()));
      }
    }
  }
}
