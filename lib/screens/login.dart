import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:provider/provider.dart';
import 'package:murtikar/screens/home.dart';
import 'package:murtikar/screens/sign_up.dart';
import 'package:murtikar/widgets/loading.dart';
import '../provider/user_provider.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();
  final _key = GlobalKey<ScaffoldState>();

  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn _googleSignIn = new GoogleSignIn();

  Future<FirebaseUser> _signIn() async {
    GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
    GoogleSignInAuthentication gSA = await googleSignInAccount.authentication;

    AuthCredential credential = GoogleAuthProvider.getCredential(
        idToken: gSA.idToken, accessToken: gSA.accessToken);

    FirebaseUser user = (await _auth.signInWithCredential(credential)).user;

    print("User Name : ${user.displayName}");
    return user;
  }

  Future<FirebaseUser> _handleSignIn() async {
    GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    GoogleSignInAuthentication googleAuth = await googleUser.authentication;
    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    final FirebaseUser user =
        (await _auth.signInWithCredential(credential)).user;
    print("signed in " + user.displayName);
    return user;
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserProvider>(context);
    return Scaffold(
      key: _key,
      body: user.status == Status.Authenticating
          ? Loading()
          : Stack(
              children: <Widget>[
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(0),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(16),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[350],
                            blurRadius:
                                20.0, // has the effect of softening the shadow
                          )
                        ],
                      ),
                      child: Form(
                          key: _formKey,
                          child: ListView(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Container(
                                    alignment: Alignment.topCenter,
                                    child: SvgPicture.asset(
                                      'assets/images/sign_in.svg',
                                      width: 260.0,
                                    )),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(
                                    14.0, 8.0, 14.0, 8.0),
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 12.0),
                                  child: SizedBox(
                                    height: 50.0,
                                    child: TextFormField(
                                      controller: _emailController,
                                      decoration: InputDecoration(
                                        prefixIcon: Icon(Icons.alternate_email),
                                        fillColor: Colors.grey.withOpacity(0.2),
                                        filled: true,
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                          const Radius.circular(8.0),
                                        )),
                                        hintText: "Email",
                                      ),
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          Pattern pattern =
                                              r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                          RegExp regex = new RegExp(pattern);
                                          if (!regex.hasMatch(value))
                                            return 'Please make sure your email address is valid';
                                          else
                                            return null;
                                        }
                                      },
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(
                                    14.0, 8.0, 14.0, 8.0),
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 12.0),
                                  child: SizedBox(
                                    height: 50.0,
                                    child: TextFormField(
                                      controller: _passwordController,
                                      decoration: InputDecoration(
                                        prefixIcon: Icon(Icons.lock_outline),
                                        fillColor: Colors.grey.withOpacity(0.2),
                                        filled: true,
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                          const Radius.circular(8.0),
                                        )),
                                        hintText: "Password",
                                      ),
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return "The password field cannot be empty";
                                        } else if (value.length < 6) {
                                          return "the password has to be at least 6 characters long";
                                        }
                                        return null;
                                      },
                                    ),
                                  ),
                                ),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        8.0, 8.0, 40.0, 8.0),
                                    child: Text(
                                      "Forgot password?",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(
                                    100.0, 8.0, 100.0, 8.0),
                                child: Material(
                                    borderRadius: BorderRadius.circular(20.0),
                                    color: Colors.black,
                                    elevation: 0.0,
                                    child: MaterialButton(
                                      onPressed: () async {
                                        if (_formKey.currentState.validate()) {
                                          if (await user.signIn(
                                              _emailController.text,
                                              _passwordController.text)) {
                                            Navigator.of(context)
                                                .pushReplacement(
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            HomePage()));
                                          } else {
                                            _key.currentState.showSnackBar(SnackBar(
                                                content: Text(
                                                    "Wrong Email or Password!")));
                                          }
                                        }
                                      },
                                      minWidth:
                                          MediaQuery.of(context).size.width,
                                      child: Text(
                                        "Login",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20.0),
                                      ),
                                    )),
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: InkWell(
                                          onTap: () {
                                            _formKey.currentState.reset();
                                            Navigator.pushReplacement(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        SignUp()));
                                          },
                                          child: Text(
                                            "Don\'t have account? Create an account",
                                            textAlign: TextAlign.center,
                                            style:
                                                TextStyle(color: Colors.black),
                                          ))),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(
                                    8.0, 8.0, 8.0, 8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        "or sign in with",
                                        style: TextStyle(
                                            fontSize: 18, color: Colors.grey),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: MaterialButton(
                                          onPressed: () => _handleSignIn()
                                              .then((FirebaseUser user) =>
                                                  print(user))
                                              .catchError((e) => print(e)),
                                          child: Image.asset(
                                            "assets/images/google.png",
                                            width: 30,
                                          )),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          )),
                    ),
                  ),
                ),
              ],
            ),
    );
  }
}

// Future handleSignIn() async {
//   preferences = await SharedPreferences.getInstance();
//   setState(() {
//     loading = true;
//   });
//   GoogleSignInAccount googleUser = await googleSignIn.signIn();
//   GoogleSignInAuthentication gSA = await googleUser.authentication;

//   AuthCredential credential = GoogleAuthProvider.getCredential(
//       idToken: gSA.idToken, accessToken: gSA.accessToken);

//   FirebaseUser user =
//       (await firebaseAuth.signInWithCredential(credential)).user;

//   if (FirebaseUser != null) {
//     final QuerySnapshot result = await Firestore.instance
//         .collection("users")
//         .where("id", isEqualTo: user.uid)
//         .getDocuments();
//     final List<DocumentSnapshot> documents = result.documents;

//     if (documents.length == 0) {
//       Firestore.instance.collection("users").document(user.uid).setData({
//         "id": user.uid,
//         "userName": user.displayName,
//         "profilePicture": user.photoUrl
//       });

//       await preferences.setString("id", user.uid);
//       await preferences.setString("userName", user.displayName);
//       await preferences.setString("profilePicture", user.photoUrl);
//     } else {
//       await preferences.setString("id", documents[0]['id']);
//       await preferences.setString("userName", documents[0]['userName']);
//       await preferences.setString(
//           "profilePicture", documents[0]['profilePicture']);
//     }
//     Fluttertoast.showToast(msg: "Logged in Successfully");
//     setState(() {
//       loading = false;
//     });
//   } else {}
// }

// Future<String> signIn(String email, String password) async {
//   AuthResult result = await firebaseAuth.signInWithEmailAndPassword(
//       email: email, password: password);
//   FirebaseUser user = result.user;
//   return user.uid;
// }
