import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:murtikar/provider/user_provider.dart';
import 'package:murtikar/screens/home.dart';
import 'package:murtikar/screens/login.dart';
import 'package:murtikar/screens/splash.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: UserProvider.initialize()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            primaryColor: Color(0xff02c39a),
            iconTheme: IconThemeData(color: Color(0xff02c39a))),
        // Color(0xffcbf3f0)
        home: ScreensController(),
      )));
}

class ScreensController extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserProvider>(context);
    switch (user.status) {
      case Status.Uninitialized:
        return Splash();
      case Status.Unauthenticated:
      case Status.Authenticating:
        return Login();
      case Status.Authenticated:
        return HomePage();
      default:
        return Login();
    }
  }
}
