import 'package:flutter/material.dart';

class SingleCartProduct extends StatelessWidget with ChangeNotifier {
  var cart_prod_id;
  var cart_prod_name;
  var cart_prod_picture;
  var cart_prod_size;
  var cart_prod_color;
  var cart_prod_price;
  var cart_prod_qty;

  SingleCartProduct(
      {this.cart_prod_id,
      this.cart_prod_name,
      this.cart_prod_color,
      this.cart_prod_picture,
      this.cart_prod_price,
      this.cart_prod_qty,
      this.cart_prod_size});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: Image.network(
          cart_prod_picture,
          height: 80.0,
          width: 80.0,
        ),
        title: Text(cart_prod_name),
        subtitle: Column(
          children: <Widget>[
            Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.fromLTRB(0.0, 5.0, 5.0, 5.0),
                        child: Text("Size:")),
                    Padding(
                        padding: const EdgeInsets.fromLTRB(0.0, 5.0, 5.0, 5.0),
                        child: Text(cart_prod_size))
                  ],
                ),
                Row(
                  children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.fromLTRB(0.0, 5.0, 5.0, 5.0),
                        child: Text("Color:")),
                    Padding(
                        padding: const EdgeInsets.fromLTRB(0.0, 5.0, 5.0, 5.0),
                        child: Text(cart_prod_color))
                  ],
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Text(
                  "\$${cart_prod_price}",
                  style: TextStyle(
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.red),
                ),
              ],
            ),
          ],
        ),
        isThreeLine: true,
        trailing: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            IconButton(icon: Icon(Icons.arrow_drop_up), onPressed: () {}),
            Text("${cart_prod_qty}"),
            IconButton(icon: Icon(Icons.arrow_drop_down), onPressed: () {}),
          ],
        ),
      ),
    );
  }
}
