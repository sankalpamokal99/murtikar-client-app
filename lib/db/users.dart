import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:murtikar/models/user.dart';

class UserServices {
  Firestore _firestore = Firestore.instance;
  String collection = "users";

  void createUser(Map data) {
    _firestore.collection(collection).document(data["userid"]).setData(data);
  }

  getUserData() async {
    String userId = (await FirebaseAuth.instance.currentUser()).uid;
    return Firestore.instance.collection('users').document(userId).snapshots();
  }
}
