import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:murtikar/models/cart_product_model.dart';
import 'package:murtikar/widgets/single_cart_product.dart';

class CartProducts extends StatefulWidget {
  String userId;
  CartProducts({@required this.userId});

  @override
  _CartProductsState createState() => _CartProductsState();
}

class _CartProductsState extends State<CartProducts> {
  List<CartProductModel> _cartProductModel = List<CartProductModel>();
  double totalPrice = 0.0;
  var cart_products = [];
  var data;
  @override
  void initState() {
    super.initState();
    getCartProductsData(widget.userId).then((cartproductsList) {
      setState(() {
        _cartProductModel.addAll(cartproductsList);
      });
    });
  }

  Future<List<CartProductModel>> getCartProductsData(String userId) async {
    var cartproductmodel = List<CartProductModel>();
    //query database
    final QuerySnapshot result = await Firestore.instance
        .collection('users')
        .document(userId)
        .collection('cart')
        .getDocuments();
    //list of snapshots
    final List<DocumentSnapshot> documents = result.documents;
    // iterate snapshots for data
    for (int i = 0; i < documents.length; i++) {
      data = documents[i].data;
      totalPrice = totalPrice + (data['price'] * data['qty']);
      cart_products.add(data);
    }

    // iterate each product
    for (var cartproductModelJson in cart_products) {
      cartproductmodel.add(CartProductModel.fromJson(cartproductModelJson));
    }
    return cartproductmodel;
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: _cartProductModel.length,
      itemBuilder: (context, index) {
        return SingleCartProduct(
          uid: widget.userId,
          cart_prod_id: _cartProductModel[index].pid,
          cart_prod_name: _cartProductModel[index].name,
          cart_prod_picture: _cartProductModel[index].picture,
          cart_prod_size: _cartProductModel[index].brand,
          cart_prod_color: _cartProductModel[index].brand,
          cart_prod_price: _cartProductModel[index].price,
          cart_prod_qty: _cartProductModel[index].qty,
        );
      },
    );
  }
}
