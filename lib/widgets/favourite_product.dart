import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:murtikar/screens/product_details.dart';

class FavouriteProduct extends StatefulWidget with ChangeNotifier {
  var userid;
  var fav_prod_id;
  var fav_prod_name;
  var fav_prod_picture;
  var fav_prod_price;
  var fav_prod_brand;

  FavouriteProduct({
    this.userid,
    this.fav_prod_id,
    this.fav_prod_name,
    this.fav_prod_picture,
    this.fav_prod_price,
    this.fav_prod_brand,
  });

  @override
  _FavouriteProductState createState() => _FavouriteProductState();
}

class _FavouriteProductState extends State<FavouriteProduct> {
  var removed = false;
  removeFromFav(String uuid, String ppid) async {
    await Firestore.instance
        .collection('users')
        .document(uuid)
        .collection('fav')
        .document(ppid)
        .delete();
    setState(() {
      removed = true;
    });
    return true;
  }

  addToFav() async {
    await Firestore.instance
        .collection('users')
        .document(widget.userid)
        .collection('fav')
        .document(widget.fav_prod_id)
        .setData({
      'uid': widget.userid,
      'pid': widget.fav_prod_id,
      'price': widget.fav_prod_price,
      'picture': widget.fav_prod_picture,
      'name': widget.fav_prod_name,
      'brand': widget.fav_prod_brand,
    });
    setState(() {
      removed = false;
    });
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => ProductDetails(
                pid: widget.fav_prod_id,
                uid: widget.userid,
                name: widget.fav_prod_name,
                price: widget.fav_prod_price,
                picture: widget.fav_prod_picture,
                brand: widget.fav_prod_brand,
                onSale: false)));
      },
      child: Card(
        child: ListTile(
          leading: Image.network(
            widget.fav_prod_picture,
            height: 80.0,
            width: 80.0,
          ),
          title: Text(widget.fav_prod_name),
          subtitle: Column(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                          padding:
                              const EdgeInsets.fromLTRB(0.0, 5.0, 5.0, 5.0),
                          child: Text("Brand:")),
                      Padding(
                          padding:
                              const EdgeInsets.fromLTRB(0.0, 5.0, 5.0, 5.0),
                          child: Text(widget.fav_prod_brand))
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Padding(
                          padding:
                              const EdgeInsets.fromLTRB(0.0, 5.0, 5.0, 5.0),
                          child: Text("Brand:")),
                      Padding(
                          padding:
                              const EdgeInsets.fromLTRB(0.0, 5.0, 5.0, 5.0),
                          child: Text(widget.fav_prod_brand))
                    ],
                  ),
                ],
              ),
              Container(
                alignment: Alignment.topLeft,
                child: Text(
                  "\$${widget.fav_prod_price}",
                  style: TextStyle(
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.red),
                ),
              ),
            ],
          ),
          trailing: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              removed
                  ? IconButton(
                      icon: Icon(
                        Icons.favorite_border,
                        color: Colors.red,
                      ),
                      onPressed: () {
                        addToFav();
                      })
                  : IconButton(
                      icon: Icon(
                        Icons.favorite,
                        color: Colors.red,
                      ),
                      onPressed: () {
                        removeFromFav(widget.userid, widget.fav_prod_id);
                      },
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
