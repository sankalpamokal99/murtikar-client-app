import 'package:flutter/material.dart';
import 'package:murtikar/screens/product_details.dart';

class ProductCard extends StatelessWidget {
  final String name;
  final double price;
  final String picture;
  final String brand;
  final bool onSale;
  final String uid;
  final String pid;

  ProductCard(
      {@required this.uid,
      @required this.pid,
      @required this.name,
      @required this.price,
      @required this.picture,
      @required this.brand,
      @required this.onSale});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => ProductDetails(
                  uid: uid,
                  pid: pid,
                  brand: brand,
                  name: name,
                  price: price,
                  picture: picture,
                  onSale: onSale,
                )));
      },
      child: Container(
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.network(
                  '$picture',
                  height: 90,
                  width: 70,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            RichText(
              text: TextSpan(children: [
                TextSpan(
                  text: '$name \n',
                  style: TextStyle(fontSize: 20),
                ),
                TextSpan(
                  text: 'by: $brand \n',
                  style: TextStyle(fontSize: 16, color: Colors.grey),
                ),
                TextSpan(
                  text: '\$${price.toString()} \t',
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                ),
                TextSpan(
                  text: 'ON SALE ',
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      color: Colors.red),
                ),
              ], style: TextStyle(color: Colors.black)),
            )
          ],
        ),
      ),
    );
  }
}
