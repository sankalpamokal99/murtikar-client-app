import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:murtikar/screens/product_details.dart';

class SingleCartProduct extends StatefulWidget with ChangeNotifier {
  var cart_prod_id;
  var cart_prod_name;
  var cart_prod_picture;
  var cart_prod_size;
  var cart_prod_color;
  var cart_prod_price;
  var cart_prod_qty;
  var uid;
  SingleCartProduct(
      {this.uid,
      this.cart_prod_id,
      this.cart_prod_name,
      this.cart_prod_color,
      this.cart_prod_picture,
      this.cart_prod_price,
      this.cart_prod_qty,
      this.cart_prod_size});

  @override
  _SingleCartProductState createState() => _SingleCartProductState();
}

class _SingleCartProductState extends State<SingleCartProduct> {
  bool removed = false;
  removeFromCart() async {
    await Firestore.instance
        .collection('users')
        .document(widget.uid)
        .collection('cart')
        .document(widget.cart_prod_id)
        .delete();
    setState(() {
      removed = true;
    });
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 12.0,
        ),
        InkWell(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => ProductDetails(
                    uid: widget.uid,
                    pid: widget.cart_prod_id,
                    name: widget.cart_prod_name,
                    price: widget.cart_prod_price,
                    picture: widget.cart_prod_picture,
                    brand: widget.cart_prod_color,
                    onSale: false)));
          },
          child: Card(
            elevation: 3.0,
            child: Container(
              color: Colors.white,
              margin: EdgeInsets.symmetric(vertical: 4.0),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 80.0,
                    height: 80.0,
                    decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.circular(20.0)),
                    child: Center(
                      child: Container(
                        width: 80.0,
                        height: 80.0,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: NetworkImage(widget.cart_prod_picture),
                                fit: BoxFit.cover),
                            borderRadius: BorderRadius.circular(5.0)),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 12.0,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              width: 100.0,
                              child: Text(
                                widget.cart_prod_name,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20.0),
                              ),
                            ),
                            Spacer(),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  0.0, 12.0, 12.0, 0.0),
                              child: Container(
                                width: 20.0,
                                height: 20.0,
                                child: InkWell(
                                  onTap: () {
                                    removeFromCart();
                                    setState(() {});
                                  },
                                  child: Icon(
                                    removed
                                        ? Icons.delete_outline
                                        : Icons.delete,
                                    color: Colors.red,
                                    size: 25.0,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Container(
                          width: 100.0,
                          child: Text(
                            "Color: " + "${widget.cart_prod_color}",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 12.0,
                                color: Colors.grey),
                          ),
                        ),
                        SizedBox(
                          height: 3.0,
                        ),
                        Container(
                          width: 100.0,
                          child: Text(
                            "Size: " + "${widget.cart_prod_size}",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 12.0,
                                color: Colors.grey),
                          ),
                        ),
                        SizedBox(
                          height: 1.0,
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              width: 20.0,
                              height: 20.0,
                              decoration: BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.circular(4.0)),
                              child: Icon(
                                Icons.remove,
                                color: Colors.white,
                                size: 15.0,
                              ),
                            ),
                            Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8.0),
                                child: Text(
                                  widget.cart_prod_qty.toString(),
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold),
                                )),
                            Container(
                              width: 20.0,
                              height: 20.0,
                              decoration: BoxDecoration(
                                  color: Colors.blue,
                                  borderRadius: BorderRadius.circular(4.0)),
                              child: Icon(
                                Icons.add,
                                color: Colors.white,
                                size: 15.0,
                              ),
                            ),
                            Spacer(),
                            Padding(
                              padding: const EdgeInsets.all(14.0),
                              child: Text(
                                "\$${widget.cart_prod_price}",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.red),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
