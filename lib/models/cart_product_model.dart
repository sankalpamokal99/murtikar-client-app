import 'dart:convert';

List<CartProductModel> cartProductModelFromJson(String str) =>
    List<CartProductModel>.from(
        json.decode(str).map((x) => CartProductModel.fromJson(x)));

String cartProductModelToJson(List<CartProductModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CartProductModel {
  String uid;
  bool sale;
  double price;
  int qty;
  String name;
  String pid;
  String brand;
  String picture;

  CartProductModel({
    this.uid,
    this.sale,
    this.price,
    this.qty,
    this.name,
    this.pid,
    this.brand,
    this.picture,
  });

  factory CartProductModel.fromJson(Map<String, dynamic> json) =>
      CartProductModel(
        uid: json["uid"],
        sale: json["sale"],
        price: json["price"],
        qty: json["qty"],
        name: json["name"],
        pid: json["pid"],
        brand: json["brand"],
        picture: json["picture"],
      );

  Map<String, dynamic> toJson() => {
        "uid": uid,
        "sale": sale,
        "price": price,
        "qty": qty,
        "name": name,
        "pid": pid,
        "brand": brand,
        "picture": picture,
      };
}
