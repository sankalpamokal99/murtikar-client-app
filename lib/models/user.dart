import 'package:cloud_firestore/cloud_firestore.dart';

class User {
  static const UID = "uid";
  static const NAME = "category";
  static const EMAIL = "name";
  static const MOBILE = "mobile";

  String _uid;
  String _name;
  String _email;
  String _mobile;

//  getters
  String get name => _name;
  String get uid => _uid;
  String get email => _email;
  String get mobile => _mobile;

//  named constructure
  User.fromSnapshot(DocumentSnapshot snapshot) {
    Map data = snapshot.data;
    _name = data[NAME];
    _uid = data[UID];
    _email = data[EMAIL];
    _mobile = data[MOBILE];
  }
}
