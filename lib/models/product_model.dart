import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

List<ProductModel> productModelFromJson(String str) => List<ProductModel>.from(
    json.decode(str).map((x) => ProductModel.fromJson(x)));

String productModelToJson(List<ProductModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ProductModel {
  bool featured;
  List<String> images;
  bool sale;
  int quantity;
  List<String> sizes;
  double price;
  String name;
  String pid;
  String category;
  String brand;
  List<String> colors;

  ProductModel({
    this.featured,
    this.images,
    this.sale,
    this.quantity,
    this.sizes,
    this.price,
    this.name,
    this.pid,
    this.category,
    this.brand,
    this.colors,
  });

  factory ProductModel.fromJson(Map<String, dynamic> json) => ProductModel(
        featured: json["featured"],
        images: List<String>.from(json["images"].map((x) => x)),
        sale: json["sale"],
        quantity: json["quantity"],
        sizes: List<String>.from(json["sizes"].map((x) => x)),
        price: json["price"],
        name: json["name"],
        pid: json["id"],
        category: json["category"],
        brand: json["brand"],
        colors: List<String>.from(json["colors"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "featured": featured,
        "images": List<dynamic>.from(images.map((x) => x)),
        "sale": sale,
        "quantity": quantity,
        "sizes": List<dynamic>.from(sizes.map((x) => x)),
        "price": price,
        "name": name,
        "id": pid,
        "category": category,
        "brand": brand,
        "colors": List<dynamic>.from(colors.map((x) => x)),
      };
//remove item from cart and fav
  removeItem(String uuid, String ppid, String collection) async {
    await Firestore.instance
        .collection('users')
        .document(uuid)
        .collection(collection)
        .document(ppid)
        .delete()
        .catchError((e) {
      return e;
    });
  }
}
