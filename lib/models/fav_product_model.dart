import 'dart:convert';

List<FavProductModel> favProductModelFromJson(String str) =>
    List<FavProductModel>.from(
        json.decode(str).map((x) => FavProductModel.fromJson(x)));

String favProductModelToJson(List<FavProductModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FavProductModel {
  String uid;
  double price;
  String name;
  String pid;
  String brand;
  String picture;

  FavProductModel({
    this.uid,
    this.price,
    this.name,
    this.pid,
    this.brand,
    this.picture,
  });

  factory FavProductModel.fromJson(Map<String, dynamic> json) =>
      FavProductModel(
        uid: json["uid"],
        price: json["price"],
        name: json["name"],
        pid: json["pid"],
        brand: json["brand"],
        picture: json["picture"],
      );

  Map<String, dynamic> toJson() => {
        "uid": uid,
        "price": price,
        "name": name,
        "pid": pid,
        "brand": brand,
        "picture": picture,
      };
}
